from flask import Flask, jsonify, request, abort
import requests
import json
from config import headers, conn, endpoint
from collections import namedtuple

unbabel = Flask(__name__)


@unbabel.route('/translate',methods=['GET', 'POST'])
def translate():
    try:
        #getting data from http request
        text_to_translate = json.loads(request.data)
        data = {'text': text_to_translate['keyword'],
               'source_language': 'en',
               'target_language': 'es',
               'text_format': 'text'
               }
        # requesting translation from api
        r = requests.post(endpoint+"/translation/", data=json.dumps(data), headers=headers)
        received = json.loads(r.text , object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        cur = conn.cursor()
        #inserting Api response in DB
        cur.execute("""INSERT INTO requests (order_number, balance, client, price, source_language, status, target_language, text, text_format, uid, translated_text) 
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",  (received.order_number, 0.0 , "", received.price, received.source_language, received.status, received.target_language,received.text,received.text_format,received.uid, "",))
        conn.commit()
        cur.close()

        return jsonify(json.loads(r.text))
    except:
      abort("error", 400)


@unbabel.route('/check',methods=['GET', 'POST'])
def check():
    try:
        # getting data from http request
        text_to_translate = json.loads(request.data)
        # checking translation status from api
        r = requests.get(endpoint+"/translation/"+text_to_translate['uid'], headers=headers)
        received = json.loads(r.text, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))
        cur = conn.cursor()
        # updating Api response in DB
        if hasattr(received, 'translatedText'):
            cur.execute(""" UPDATE requests set  status = %s , translated_text = %s WHERE uid = %s """, (received.status, received.translatedText,text_to_translate['uid'],))
        else:
            cur.execute(""" UPDATE requests set  status = %s   WHERE uid = %s""", (received.status,text_to_translate['uid'],))
        conn.commit()
        cur.close()
        return jsonify(json.loads(r.text))
    except ValueError:
        abort("error", 400)







if __name__ == '__main__':
    unbabel.run()
