import psycopg2

conn = psycopg2.connect(host='localhost', user='postgres', password='root', dbname='unbabel', port='5432' )
headers = {
'Authorization': 'ApiKey fullstack-challenge:9db71b322d43a6ac0f681784ebdcc6409bb83359',
'Content-Type': 'application/json'
}
endpoint = "https://sandbox.unbabel.com/tapi/v2"
