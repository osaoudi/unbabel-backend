import unittest
import unbabel
import requests
import json
import sys



class TestFlaskApi(unittest.TestCase):
    def setUp(self):
        self.app = unbabel.unbabel.test_client()

    def test_check(self):
        response = self.app.post('/check', data='{"uid":"f3b68c6db5"}')
        self.assertEqual(json.loads(response.get_data()), {'order_number':36454, 'price':3.0, 'source_language':'en', 'status':'completed', 'target_language':'es', 'text':'garden','text_format':'text', 'translatedText':'jardÃ\xadn','uid':'f3b68c6db5'})


    def test_translate(self):
        response = self.app.post('/translate', data='{"keyword":"hello"}')
        self.assertNotEqual(json.loads(response.get_data()), {'order_number':36457, 'price':3.0, 'source_language':'en', 'status':'new', 'target_language':'es', 'text':'hello', 'text_format':'text', 'uid':'2634e62b6f'})

if __name__ == "__main__":
    unittest.main()
